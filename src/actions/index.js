//action to fetch posts
import axios from "axios";

import { FETCH_POSTS, CREATE_POST, FETCH_POST, DELETE_POST } from "./types";
import { URL, BASE_URL, API_KEY } from "./endpoints";



export function fetchPosts() {
    const request = axios.get(URL);

    return {
        type: FETCH_POSTS,
        payload: request
    }
}

export function createPost(props) {
    //props arguments will be:title,categories,content

    const request = axios.post(URL, props);

    return {
        type: CREATE_POST,
        payload: request
    }

}

export function fetchPost(id) {
    const url = `${BASE_URL}posts/${id}/?key=${API_KEY}`;
    const request = axios.get(url);

    return {
        type: FETCH_POST,
        payload: request
    }
}

export function deletePost(id) {
    const url = `${BASE_URL}posts/${id}/?key=${API_KEY}`;

    const request = axios.delete(url);
    return {
        type: DELETE_POST,
        payload: request
    }
}