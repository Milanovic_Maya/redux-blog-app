import React, { Component } from 'react';
import './css/App.css';
import Main from "./components/Main";
import { Route, Switch } from "react-router-dom";
import IndexPage from './components/IndexPage';
import NewPost from "./components/NewPost";
import PostDetails from "./components/PostDetails";

class App extends Component {

  render() {
    return (
      <div className="App">
        <Switch>
          <Route path="/posts/new" component={NewPost} />
          <Route path="/posts/:id" component={PostDetails} />
          <Route exact path="/" component={Main} />
          <Route component={IndexPage} />
        </Switch>
      </div>
    );
  }
}

export default App;
