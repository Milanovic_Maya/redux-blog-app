import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import "../css/form.css";
import InputText from "./InputText";
import InputTextarea from "./Textarea";
import { createPost } from "../actions/index";
import { connect } from "react-redux";
import { Link } from "react-router-dom";


class NewPost extends Component {

    onSubmit = (values) => {
        this.props.createPost(values)
            //action creator returns a promise, so we use then()
            .then(() => {
                this.props.history.push("/");
            });
    }

    render() {
        const { handleSubmit } = this.props;

        return (
            <div className="container">
                <form onSubmit={handleSubmit(this.onSubmit)}>
                    <div>
                        <h3>Create new post</h3>
                        <label htmlFor="title">Title:</label>
                        <Field name="title"
                            component={InputText}
                            type="text"
                        />
                    </div>
                    <div>
                        <label htmlFor="categories">Categories:</label>
                        <Field name="categories"
                            component={InputText}
                            type="text"
                        />

                    </div>
                    <div>
                        <label htmlFor="content">Content:</label>
                        <Field name="content"
                            component={InputTextarea}
                        />
                    </div>
                    <div className="btn-holder">
                        <button type="submit">Post</button>
                        <Link to="/"><button>Cancel</button></Link >
                    </div>

                </form>
            </div>
        );
    };
};

//function for form validation:
const validate = values => {
    const errors = {}
    if (!values.title) {
        errors.title = "Title is required.";
    }
    if (!values.categories) {
        errors.categories = "Required at least one category";
    }
    if (!values.content) {
        errors.content = "Required some content";
    }
    return errors
}

//configuration for form:

NewPost = reduxForm({
    form: "CreateNewPost",
    // initialValues: {
    //     title: "My Post",
    //     categories: "cats",
    //     content: "My Cats love to..."
    // },
    validate
})(NewPost);

export default connect(null, { createPost })(NewPost);

