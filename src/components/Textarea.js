import React from 'react'

const InputTextarea = ({ input, label, meta: { touched, error } }) => {
    return (<div>
        <textarea {...input} cols="30" rows="10" placeholder="Post Content..." />
        {/*condition for showing error*/}
        {touched && error && <p className="error-p">{error}</p>}
    </div>)
}

export default InputTextarea;