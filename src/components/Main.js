import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchPosts } from "../actions/index";
import { Link } from "react-router-dom";
import "../css/main.css";

class Main extends Component {

    renderPosts() {
        return this.props.posts.map(post => {
            return (
                <Link to={`/posts/${post.id}`} key={post.id}>
                    <li>
                        <p className="post-categories">Categories: {post.categories}</p>
                        Title:<strong> {post.title}</strong>
                    </li>
                </Link>
            )
        })
    }


    componentDidMount() {
        this.props.fetchPosts();
    }

    render() {
        return (
            <div className="container">
                <Link to="/posts/new">
                    <button>
                        Add Post
                    </button>
                </Link>
                {/*display post list*/}
                <h3>Posts</h3>
                <ul>
                    {this.renderPosts()}
                </ul>
            </div>

        )
    }
}

const mapStateToProps = (state) => {
    return { posts: state.posts.all }
}

const mapDispatchToProps = (dispatch) => {
    return {
        //don't need bindActionCreators({fetchPosts},dispatch);
        //now it's anon.function which does that
        fetchPosts: () => dispatch(fetchPosts())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);