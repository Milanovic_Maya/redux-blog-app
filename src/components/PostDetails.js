import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { fetchPost, deletePost } from "../actions/index";
import "../css/postDetails.css";

class PostDetails extends Component {

    componentDidMount() {
        //fetch post
        const { fetchPost, match: { params: { id } } } = this.props;
        fetchPost(id);
    }

    onDeleteClick() {
        const { deletePost, match: { params: { id } }, history } = this.props;
        deletePost(id)
            .then(() => { history.push("/") });
    }

    render() {
        const { post } = this.props;
        //handle null props here:
        if (!post) {
            return <div className="loading-content">Loading...</div>
        }

        return (
            <div className="container">
                <Link to="/">{`< Back to Posts`}</Link>
                <h3>{post.title}</h3>
                <button
                    onClick={this.onDeleteClick.bind(this)}
                    className="delete-post-button">
                    Delete Post
                </button>
                <h6>Categories:{post.categories}</h6>
                <p>{post.content}</p>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return { post: state.posts.post };
};

export default connect(mapStateToProps, { fetchPost, deletePost })(PostDetails);