import React from 'react'

const InputText = ({ input, label, meta: { touched, error } }) => {
    //touched: if input's been modified, then it's true;
    return (<div>
        <input {...input} type="text" placeholder="Title..." />
        {/*condition for showing error*/}
        {touched && error && <p className="error-p">{error}</p>}
    </div>)
}

export default InputText