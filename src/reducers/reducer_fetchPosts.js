import { FETCH_POSTS, FETCH_POST } from "../actions/types";

//pure function returns new obj as piece of state

const INITIAL_STATE = {
    all: [],
    post: null
}

export function PostsReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case FETCH_POST:
            return { ...state, post: action.payload.data };
        case FETCH_POSTS:
            return { ...state, all: action.payload.data };
        default:
            return state;
    }
}