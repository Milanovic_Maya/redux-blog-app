import { combineReducers } from "redux";
import { PostsReducer } from "./reducer_fetchPosts";
import { reducer as formReducer } from "redux-form";

export default combineReducers({
    form: formReducer,
    posts: PostsReducer
});