This is simple app made for the purpose of learning Redux, ReactRedux, ReactRouter, ReduxForm.

App is bootstrapped with CRA. Sass was used for style.

Tutorial : Udemy, Modern React with Redux, by Stephen Grider.